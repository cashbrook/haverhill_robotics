Cliff Ashbrook

Topic: Haverhill High School Robotics Club Website

Files:
/robotics
    - index.html: Home page
    - about.html: Information about the club
    - competitions.html: Information about about competitions
    - resources.html: Organized list of resources for programming, web development and 3D modeling
    - contact.html: Form inputs to contact the club
    - style.css: CSS file for Haverhill Robotics website
    - README.txt: description of website and files
    - /images
        - botball_logo.png: Logo for Botball competition (Courtesy of KIPR.org)
        - botball_table.jpg: index.html picture 
        - club_group.jpg: about.html yearbook picture
        - haverhill_robotics_logo.png: Logo for club and website
        - zero_logo: Logo for Zero Robotics competition (Courtesy of zerorobotics.mit.edu)